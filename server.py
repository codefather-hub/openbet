from aiohttp import web
from parseapp.main import Processor
import aiohttp_jinja2
import jinja2

routes = web.RouteTableDef()

@routes.get('/api/v1/get_updates/{param}', name='get_updates')
async def sports(request):
	'''
	: upcoming_odds, sports
	'''
	param = request.match_info['param']
	return web.json_response(Processor.get_updates(param))


@routes.get('/', name='base')
@aiohttp_jinja2.template('base.html')
async def main(request):
    return {}


@routes.get('/odds', name='odds')
@aiohttp_jinja2.template('odds.html')
async def main(request):
    return {}

app = web.Application()
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))
app.add_routes(routes)
web.run_app(app)