from django.shortcuts import render
# from bk_scrapers.bk_scrapers.spiders import leon_scraper
# from bk_scrapers.bk_scrapers import settings


# Create your views here.

def base(request):
    host = request.META['HTTP_HOST']
    get_updates_url = f'http://{host}/api/v1/get_updates'
    return render(request, 'base.html', {
        "get_updates_url": get_updates_url,
    })