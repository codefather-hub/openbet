from django.apps import AppConfig
from suit.apps import DjangoSuitConfig

class CoreappConfig(AppConfig):
    name = 'coreapp'

class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'