from __future__ import absolute_import, unicode_literals
from celery import Celery
from celery.schedules import crontab
#from API.tasks import UpdatesHandler
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.settings")

app = Celery('settings')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    # Executes every Monday morning at 7:30 a.m.
    'crawl_main_pages': {
        'task': 'API.tasks.crawl_main_pages',
        'schedule': crontab(),
        #'args': (16, 16),
    },
    'crawl_events_pages': {
        'task': 'API.tasks.crawl_events_pages',
        'schedule': crontab(),
        #'args': (16, 16),
    },
}

app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))