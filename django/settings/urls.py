from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    
    #API URLS
    path('api/v1/', include(('API.urls', 'API'), namespace='api-urls')),
    
    #CORE URLS
    path('', include(('coreapp.urls', 'coreapp'), namespace='core-urls')),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
