# -*- coding: utf-8 -*-
import scrapy
import os
from odds.spiders.event_spider_utils import (
    parse_football, parse_other_games
)
from .event_spider_utils import (
    get_game_from_url, get_league_from_url
)


class EventSpiderSpider(scrapy.Spider):
    name = 'event_spider'
    allowed_domains = ['*']
    urls = []
    path = os.getcwd() + '/' + 'pages/'
    dirs = os.listdir(path)
    for _dir in dirs:
    	urls.append(f'file://{path}{_dir}/main.html')
    start_urls = urls

    def start_requests(self):
    	for u in self.start_urls:
    		yield scrapy.Request(u, callback=self.parse)
    
    def parse(self, response):

        '''
        Нужно попробовать все фикстуры вытащить из ифа
        слишком подозрительно
        '''
        page_centre_container = response.xpath('//div[@id="page-centre-container"]')
        container = page_centre_container.xpath('./div[@id="container"]')
        wrapper = container.xpath('./div[@id="wrapper"]')
        wrapper_inner = wrapper.xpath('./div[@id="wrapper-inner"]')
        wrapper_content = wrapper_inner.xpath('./div[@id="wrapper-content"]')
        main_content = wrapper_content.xpath('./div[@id="main-content"]')
        # 39 mc
        mc = main_content.xpath('./div[@id="mc"]')
        #print(mc)
        section_2 = mc.xpath('./section[2]')
        #print(section_2)
        
        # 1 ветка
        content = mc.xpath('./div[@class="content"]')
        inner_content = content.xpath('./div[@class="inner-content"]')
        event = inner_content.xpath('./div[@class="event"]')
        p = event.xpath('./p')
        # print(event)
        # print(p)
        
        
        # 2 ветка
        page_module_football_module = mc.xpath('./section[@class="page-module football-module"]')
        if page_module_football_module:
    		# Найден футбол
            # yield parse_football(self, page_module_football_module)

            # 16 fixtures
            fixtures = page_module_football_module.xpath('./div[@id="fixtures"]')
            # 16 content 4
            content_4 = fixtures.xpath('./div[@class="content-4"]')
            # 16 tables
            table = content_4.xpath('./table')
            # print(table)
            
        # Ветка others
        outrights = section_2.xpath('./div[@id="outrights"]')
        if outrights:
            # Athletics, Chess, Surfing, Pool, Netball
            yield parse_other_games(self, outrights)
