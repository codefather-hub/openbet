import requests
import re

def get_league_from_url(url):
    url = url.split('/')
    _1step = ['gaelic-games',]
    _2step = ['tennis', 'american-football', 'baseball', 'handball'
              'boxing', 'ufc-mma', 'australian-rules', 'badminton',
              'bowls', 'rugby-union', 'pool', 'surfing', 'athletics' ,
              'chess']
    _3step = ['cricket', 'football', 'rugby-league' , 'netball']
    for line in _1step:
        if line in url:
            return re.sub('-', '_', url[1])
  
    for line in _2step:
        if line in url:
            return re.sub('-', '_', url[2])
        
    for line in _3step:
        if line in url:
            return re.sub('-', '_', url[3])

def get_country_from_url(url):
    if 'english' in url or 'england' in url:
        return "england"
    if 'usa' in url:
        return "usa"
    if 'portugal' in url:
        return "portugal"
    if 'netherlands' in url:
        return "netherlands"
    if 'scottish' in url:
        return "scotland"
    if 'italy' in url:
        return "italy"
    if 'spain' in url:
        return "spain"
    if 'ireland' in url:
        return "ireland"
    if 'brazil' in url:
        return "brazil"
    if 'australia' in url or 'australian' in url:
        return "australia"

def get_game_from_url(url):
    if 'chess' in url:
        return "chess"
    if 'athletics' in url:
        return "athletics"
    if 'surfing' in url:
        return "surfing"
    if 'netball' in url:
        return "netball"
    if 'pool' in url:
        return "pool"
    if 'american-football' in url:
        return "american_football"
    if 'gaelic-football' in url:
        return "gaelic_football"
    if 'hurling' in url:
        return "hurling"
    if 'football' in url:
        return "football"
    if 'cricket' in url:
        return "cricket"
    if 'table-tennis' in url:
        return "table-tennis"
    if 'handball' in url:
        return "handball"
    if 'tennis' in url:
        return "tennis"
    if 'basketball' in url:
        return "basketball"
    if 'baseball' in url:
        return "baseball"
    if 'rugby' in url:
        return "rugby"
    if 'rules' in url:
        return "rules"
    if 'boxing' in url:
        return "boxing"
    return url

def send(send_object, param):
    if param == 'events/others':
        requests.post('http://127.0.0.1:8000/api/v1/get_events_others', json=send_object)
    if param == 'events':
        requests.post('http://127.0.0.1:8000/api/v1/get_events', json=send_object)
    if param == 'leagues':
        requests.post('http://127.0.0.1:8000/api/v1/get_leagues', json=send_object)

def parse_other_games(self, response):
	outrights = response
	main_outright = outrights.xpath('./div[@class="main-outright"]')
	event_objects = []
	for event in main_outright:
		
		content_4 = event.xpath('./div')
		h3 = content_4.xpath('./h3')
		h3_title = h3.xpath('./a/@title').extract_first()
		h3_href = h3.xpath('./a/@href').extract_first()
		
		odds_objects = []
		for li in content_4.xpath('./ul[@class="eventTable"]/li'):
			odd = li.xpath('./span/text()').extract_first()
			a_href = li.xpath('./a/@href').extract_first()
			a_title = li.xpath('./a/@title').extract_first()
			
			odd_object = {
				"odd-title": a_title,
				"odd-type": "winner",
				"odd-url": a_href,
				"odd": odd,
			}
			
			odds_objects.append(odd_object)
		
		event_obj = {
			"event-title": h3_title,
			"event-url": h3_href,
			"event-game": get_game_from_url(h3_href),
			"event-league": get_league_from_url(h3_href),
			"odds": odds_objects
		}
		
		event_objects.append(event_obj)
	send(event_objects, param="events/others")
	print('\n \n')
	print(event_objects)
	print('\n \n')

def parse_football(self, response):
	table = response.xpath('//table')

	leagues = []
	events = []
	for event in table:
		head_tr = table.xpath('./tbody/tr[@class="hda-header mob-less-pad"]')
		body_tr = table.xpath('./tbody/tr[@class="match-on no-top-border"]')
		for line in head_tr:
			#if not "View" in line.xpath('./td/a/@title').extract_first():
			league = {
				"league-title": line.xpath('./td/a/@title').extract_first(),
    			"league-url": line.xpath('./td/a/@href').extract_first(),
       			"league-img": line.xpath('./td/a/img/@src').extract_first(),
				"league-game": get_game_from_url(line.xpath('./td/a/@href').extract_first())
			}
			leagues.append(league)
			
		for line in body_tr:
			events.append({
    			# "event-time": line.xpath('./td[@class="time all-odds-click"]/div/span/text()').extract_first(),
    			"event-players": line.xpath('./td[@class="all-odds-click"]/p/text()').extract(),
    			# "event-title": line.xpath('./td/a/@title').extract_first(),
    			"event-url": line.xpath('./td/a/@href').extract_first(),
				"event-game": get_game_from_url(line.xpath('./td/a/@href').extract_first()),
				"country": get_country_from_url(line.xpath('./td/a/@href').extract_first()),
				"league": get_league_from_url(line.xpath('./td/a/@href').extract_first())
    			# "event-h2h-odds": {
    			# 	"origin": line.xpath('./td[@class="basket-add"]/p/span/text()').extract(),
    			# 	"european": line.xpath('./td[@class="basket-add"]/@data-best-dig').extract(),
    			# 	"data-bid": line.xpath('./td[@class="basket-add"]/@data-bid').extract(),
    			# },
			})
	#send(leagues, param="leagues")
	send(events, param="events")
	#print(events)
    #yield leagues