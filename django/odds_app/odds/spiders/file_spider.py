# -*- coding: utf-8 -*-
import scrapy
import json
import codecs
import requests
import sys
import os
import re

import subprocess


class FileSpiderSpider(scrapy.Spider):
    name = 'file_spider'
    allowed_domains = ['*']
    start_urls = ['file:///home/codefather/Загрузки/Telegram Desktop/1/самые важные проекты/opensport/django/main.html']

    def mkdir(self, name):
    	try:
    		os.mkdir(name)
    	except:
    		pass

    def get_page(self, url, title):
        title = title.decode("utf-8")
        if '/' in title:
            title = re.sub('/', '-', title)
        print("TITLE:", title)
        self.mkdir('pages/{}'.format(title))
        headers = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)'}
        r = requests.get(url, headers=headers).text
        f = open('pages/{}/main.html'.format(title), 'w')
        f.write(r)
        return {
            "title": title,
            "url": url,
            # "html_file": open('pages/{}/main.html'.format(title), 'r').read(),
            "html_path": "pages/{}/main.html".format(title)
        }

    def genEventSpider(self, title):
        title = re.sub(' ', '', title)
        title = re.sub('-', '', title)
        title = re.sub('/', '', title)
        print(title)
        #print("CURRENT", os.getcwd())
        #open(f'{title}Event.sh', 'w').write(f'scrapy genspider {title}Event main.html')
        #subprocess.Popen(['sh', f'./{title}Event.sh'])

    def parse(self, response):
        menu = response.xpath('//div[@id="all-sports-panel-wrap"]/div[@id="all-sports-panel-beta"]/ul/li/a')
        data = []
        for line in menu:

            category_page = self.get_page(
                url=str(line.xpath('.//@href').extract_first()).encode('utf-8'),
                title=str(line.xpath('.//text()').extract_first()).encode('utf-8')
            )
            # print("\n \n ")
            # print(category_page['title'], category_page['html_path'])
            absolute_path = "file:///home/codefather/Загрузки/Telegram Desktop/1/самые важные проекты/opensport/django/odds_app/{}".format(category_page['html_path'])
            obj = {
                "absolute_path": absolute_path,
                "category": category_page
            }

            data.append(obj)
            self.genEventSpider(obj['category']['title'])

        try:
            requests.post("http://127.0.0.1:8000/api/v1/save_main_html", json=data)
        except:
            pass

        pages_path = os.getcwd() + '/' + 'pages'
        print(os.listdir(pages_path))

        return data

