from django.db import models
from django.contrib.postgres.fields import JSONField, HStoreField
from .utils import Middleware

import datetime
# Create your models here.


class League(models.Model):
    
    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'лига'
        verbose_name_plural = 'лиги'
        
    raw = JSONField(
        null=True,
        blank=True,
        verbose_name="JSON"
    )
        
    title = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Наименование",
    )
    
    game = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Игра",
    )
    
    logo = models.URLField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Логотип",
    )
    
    url = models.URLField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Ссылка",
    )
    
    timestamp = models.DateTimeField(
        auto_now=True,
    )
    
class Player(models.Model):
    name = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Игроки",
    )
    
    game = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Игра",
    )
    
    country = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Страна",
    )
    
    league = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Лига",
    )
    
    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = 'игрок'
        verbose_name_plural = 'игроки'

class Event(models.Model):

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'событие'
        verbose_name_plural = 'события'

    raw = JSONField(
        null=True,
        blank=True,
        verbose_name="JSON"
    )

    title = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Наименование",
    )
    
    players = models.ManyToManyField(
        "API.Player",
        max_length=255,
        default=None,
        blank=True,
        verbose_name="Игроки",
    )
    
    game = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Игра",
    )
    
    country = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Страна",
    )
    
    league = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Лига",
    )
    
    url = models.URLField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Ссылка",
    )
    
    timestamp = models.DateTimeField(
        auto_now=True,
    )


class ParseData(models.Model):

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = 'данные'
        verbose_name_plural = 'данные'

    def get_raw(self):
        return self.raw

    def get_base_info(self):
        return self.raw['base_info']

    title = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        verbose_name="Наименование",
    )

    raw = JSONField(
        null=True,
        blank=True,
        verbose_name="Первичный (сырой) ответ"
    )

    main_html = models.FileField(
        max_length=600,
        upload_to='',
        default=None,
        null=True,
        blank=True,
        verbose_name="HTML Страница",
    )
    
    timestamp = models.DateTimeField(
        auto_now=True,
    )



class Response(models.Model):
    
    class Meta:
        verbose_name = 'ответ'
        verbose_name_plural = 'ответы'
    
    def __str__(self):
        return str(self.id)
    

    @Middleware.response
    def save(self, *args, **kwargs):
        return super(Response, self).save(*args, **kwargs)
    
    start = 0
    first_step = 1
    second_step = 2
    third_step = 3
    final = 4
    
    RESPONSE_STEP_CHOICES = (
        (start, "Старт"),
        (first_step, "Первый"),
        (second_step, "Второй"),
        (third_step, "Третий"),
        (final, "Все шаги пройдены")
    )
    
    type = models.CharField(
        max_length=255,
        default=None,
        null=True,
        blank=True,
        help_text="Пример, - sports, upcomming",
        verbose_name="Тип",
    ) 
    
    step = models.PositiveIntegerField(
        default=0,
        null=True,
        blank=True,
        choices=RESPONSE_STEP_CHOICES,
        verbose_name="Шаг"
    )
    
    raw = JSONField(
        null=True,
        blank=True,
        verbose_name="Первичный (сырой) ответ"
    )
    
    started = JSONField(
        null=True,
        blank=True,
        verbose_name="Стартующий ответ"
    )
    
    f_step = JSONField(
        null=True,
        blank=True,
        verbose_name="1 шаг обработки"
    )
    
    s_step = JSONField(
        null=True,
        blank=True,
        verbose_name="2 шаг обработки"
    )
    
    t_step = JSONField(
        null=True,
        blank=True,
        verbose_name="3 шаг обработки"
    )
    
    final_step = JSONField(
        null=True,
        blank=True,
        verbose_name="Все шаги пройдены"
    )
    
    current = models.BooleanField(
        default=True,
        null=True,
        blank=True,
        verbose_name="Текущий ответ"
    )
    
    is_using = models.BooleanField(
        default=True,
        null=True,
        blank=True,
        verbose_name="Используется"
    )
    
    is_used = models.BooleanField(
        default=None,
        null=True,
        blank=True,
        verbose_name="Использован"
    )
    
    is_in_queue = models.BooleanField(
        default=None,
        null=True,
        blank=True,
        verbose_name="В очереди"
    )
    
    is_with_error = models.BooleanField(
        default=None,
        null=True,
        blank=True,
        verbose_name="С ошибкой"
    )
    
    timestamp = models.DateTimeField(
        auto_now=True,
        null=True,
        blank=True,
        verbose_name="Timestamp"
    )