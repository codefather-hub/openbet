from django.urls import path
from . import views

urlpatterns = [
    path(
        'get_updates/<str:param>/',
        views.get_updates,
        name = 'get-updates'
    ),

    path(
    	'save_main_html',
    	views.save_main_html,
    	name = 'save-main-html'
    ),
    path(
        'get_leagues',
        views.get_leagues,
        name = 'get-leagues',
    ),
    path(
        'get_events',
        views.get_events,
        name = 'get-events',
    ),
    path(
        'get_events_others',
        views.get_events_others,
        name = 'get-events-others'
    )
]