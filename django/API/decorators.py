from django.http import JsonResponse

class ResponseHandler:

	def check_post(func):
		def wrapper(request, *args, **kwargs):
			if request.method == 'POST':
				return func(request, *args, **kwargs)
			else:
				return JsonResponse({
					"status": "error",
					"description": "cant use get request"
				})
		return wrapper