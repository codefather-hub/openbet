# Generated by Django 2.2.4 on 2019-08-18 15:34

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0009_league_game'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='country',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='Страна'),
        ),
        migrations.AddField(
            model_name='event',
            name='game',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='Игра'),
        ),
        migrations.AddField(
            model_name='event',
            name='league',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='Лига'),
        ),
        migrations.AddField(
            model_name='event',
            name='players',
            field=models.CharField(blank=True, default=None, max_length=255, null=True, verbose_name='Игроки'),
        ),
        migrations.AddField(
            model_name='event',
            name='raw',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='JSON'),
        ),
        migrations.AddField(
            model_name='event',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='event',
            name='url',
            field=models.URLField(blank=True, default=None, null=True, verbose_name='Ссылка'),
        ),
        migrations.AddField(
            model_name='league',
            name='raw',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='JSON'),
        ),
        migrations.AddField(
            model_name='league',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='parsedata',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
