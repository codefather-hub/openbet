from django.contrib import admin
from .models import Response, ParseData, League, Event, Player
# Register your models here.

@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'game', 'league', 'country')

@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'game', 'league', 'country', )
    filter_horizontal = ('players',)

@admin.register(League)
class LeagueAdmin(admin.ModelAdmin):
    list_display = ('id', 'game', 'title')

@admin.register(ParseData)
class ParseDataAdmin(admin.ModelAdmin):
	list_display = ('id', 'title')

@admin.register(Response)
class ResponseAdmin(admin.ModelAdmin):
    pass