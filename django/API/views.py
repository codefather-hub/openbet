from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render
from django.core.files import File

from .models import Response, ParseData
from .decorators import ResponseHandler
from .tasks import UpdatesHandler
from .utils import Processor

import subprocess
import datetime
import json
import re
import os


#import subprocess

# Create your views here.

@csrf_exempt
@ResponseHandler.check_post
def get_events_others(request):
    
    body = request.body
    decoded_body = body.decode("utf-8")
    json_response = json.loads(decoded_body)

    UpdatesHandler.parse_updates_events_others.delay(json_response)
    return JsonResponse({"status": "ok"})

@csrf_exempt
@ResponseHandler.check_post
def get_events(request):
    
    body = request.body
    decoded_body = body.decode("utf-8")
    json_response = json.loads(decoded_body)

    UpdatesHandler.parse_updates_events.delay(json_response)
    return JsonResponse({"status": "ok"})
    
@csrf_exempt
@ResponseHandler.check_post
def get_leagues(request):

    body = request.body
    decoded_body = body.decode("utf-8")
    json_response = json.loads(decoded_body)

    UpdatesHandler.parse_updates_leagues.delay(json_response)
    return JsonResponse({"status": "ok"})

@csrf_exempt
@ResponseHandler.check_post
def save_main_html(request):

    body = request.body
    decoded_body = body.decode("utf-8")
    json_response = json.loads(decoded_body)

    UpdatesHandler.save_main_html_task.delay(json_response)

    return JsonResponse({
        "status": "ok",
    })

def get_updates(request, param):
    #UpdatesHandler.crawl.delay()
    
    if Response.objects.filter(current=True, type=param).exists():
        json_response = Response.objects.filter(current=True, type=param).last().raw
        UpdatesHandler.update_raw_data.delay(
            param, str(Response.objects.filter(current=True, type=param).last().id),
        )
        return JsonResponse(json_response, safe=False)
    else:
        UpdatesHandler.update_raw_data.delay(param)
        return JsonResponse({"status": "no current data in database"})