import requests
import json


class Middleware:
    def response(func):
        def wrapper(self, *args, **kwargs):
            if self.final_step != None:
                self.is_used = True
            if self.is_used:
                self.is_using = False
                self.is_in_queue = False
            if not self.is_used and self.current:
                self.is_using = True
            if self.is_with_error:
                self.is_using = False
                self.is_in_queue = False
                self.is_used = False
                self.current = False
            if not self.current:
                self.is_using = False
                self.is_used = False
                self.is_in_queue = False
            return func(self, *args, **kwargs)
        return wrapper

class Parser:

	api_key = 'd8ee791a97ab93344fdd6b4c3a1a9429'

	def __init__(self, *args, **kwargs):
		self.api_key = Parser.api_key

	def get_sports():
		url = 'https://api.the-odds-api.com/v3/sports/?apiKey='
		return Parser.request(url)

	def get_upcoming_odds(
			sport: str = 'UPCOMING',
			region: str = None,
			odds_param: str = None
		):
		'''
		# sport: default = UPCOMING
		# region: uk, au, default = us
		# odds_param: default = h2h
		'''

		if not region:
			region = 'us'
		if not odds_param:
			odds_param = 'h2h'

		url = f'https://api.the-odds-api.com/v3/odds/?sport={sport}&region={region}&mkt={odds_param}&apiKey='
		return Parser.request(url)

	def request(url):
		return requests.get(f'{url}{Parser.api_key}').json()

class Processor:
	def __init__(self, *args, **kwargs):
		pass


	def get_updates(param: str):

		updates = []

		if param == 'sports':
			sports = Parser.get_sports()

			if Processor.response_is_valid(sports):
				updates.append({
					"sports": sports['data']
				})

		if param == 'upcoming_odds':
			upcoming_odds = Parser.get_upcoming_odds()


			if Processor.response_is_valid(upcoming_odds):
				updates.append({
					"upcoming_odds": upcoming_odds['data']
				})


		return json.loads(json.dumps(updates, indent=4))

	def response_is_valid(response):
		if not response['success']:
		    return False
		else:
		    return True