from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .utils import Processor
from .models import Response, ParseData, League, Event, Player
from django.core.files import File
import subprocess
from requests import post

import datetime
import json
import re
import os

class UpdatesHandler:

    @shared_task
    def save_main_html_task(json_response):
        for line in json_response:
            absolute_path = line['absolute_path']
            title = line['category']['title']
            url = line['category']['url']

            absolute_path = re.sub('file://', '', absolute_path)
            created_file = open(f'media/{title}.html', 'w')
            created_file.write(open(absolute_path, 'r').read())

            file = open(f'media/{title}.html', 'r')
            file = File(file)

            if not ParseData.objects.filter(title=title).exists():
                ParseData.objects.create(
                    title=title,
                    main_html=file,
                    raw={
                        "base_info": [{
                            "object": "file",
                            "format": "html",
                            "type": "main_html",
                            "path": str(absolute_path),
                            "title": str(title),
                            "victim_url": str(url),
                            "created": str(datetime.datetime.now())
                        }],
                    }
                )
            else:
                obj = ParseData.objects.get(title=title)
                os.remove(obj.main_html.path)
                obj.main_html = None
                obj.save()

                obj.main_html=file
                obj.raw['base_info'][0]['path'] = str(absolute_path)
                obj.raw['base_info'][0]['victim_url'] = str(url)
                obj.raw['base_info'][0]['updated'] = str(datetime.datetime.now())
                obj.save()

    @shared_task
    def parse_updates_events_others(json_dict):
        event_title = json_dict[0]['event-title']
        event_url = json_dict[0]['event-url']
        event_game = json_dict[0]['event-game']
        event_league = json_dict[0]['event-league']
        odds = json_dict[0]['odds']
        
        if not Event.objects.filter(
            title=event_title,
            game=event_game,
            league=event_league,
            url=event_url,
        ).exists():
            l = League.objects.create(
                title=event_league,
                game=event_game,
            )
            e = Event.objects.create(
                raw=json_dict,
                title=event_title,
                game=event_game,
                league=event_league,
                url=event_url,
            )
        
            print(event_title, event_url, event_game, event_league, odds)
            for line in odds:
                odd_title = line['odd-title']
                odd_url = line['odd-url']
                odd_type = line['odd-type']
                odd = line['odd']
                e.players.add(
                    Player.objects.create(
                        name=odd_title,
                        game=event_game,
                        league=event_league
                    )
                )
                e.save()
                print(odd_title, odd_url, odd_type, odd)

    @shared_task
    def parse_updates_events(json_dict):
        for line in json_dict:
            event_players = line['event-players']
            event_url = line['event-url']
            event_game = line['event-game']
            country = line['country']
            league = line['league']
            
            if not Event.objects.filter(
                    title=f'{country} {event_game} {league}',
                    players=event_players,
                    game=event_game,
                    country=country,
                    league=league,
                    url=event_url,
                ).exists():
                e = Event.objects.create(
                    raw=json_dict,
                    title=f'{country} {event_game} {league}',
                    players=event_players,
                    game=event_game,
                    country=country,
                    league=league,
                    url=event_url,
                )
                print(e)
                
            

    @shared_task
    def parse_updates_leagues(json_dict):
        for line in json_dict:
            title = line['league-title']
            url = line['league-url']
            img = line['league-img']
            game = line['league-game']
            print(title)
            print(url)
            print(img)
            
            if not League.objects.filter(
                title=title,
                url=url,
                game=game,
            ).exists():
                
                l = League.objects.create(
                    title=title,
                    url=url,
                    game=game,
                    logo=img,
                )
                
                print(l)

    @shared_task
    def crawl_events_pages():
        data = {"project": "default", "spider": "event_spider"}
        url = 'http://localhost:6800/schedule.json'
        response = post(url, data=data)
        print(response)

    @shared_task
    def crawl_main_pages():
        data = {"project": "default", "spider": "file_spider"}
        url = 'http://localhost:6800/schedule.json'
        response = post(url, data=data)
        print(response)
    
    @shared_task
    def update_raw_data(param, id=None):
        json_response = Processor.get_updates(param)
        new_current_response = Response.objects.create(
            raw=json_response,
            type=param,
        )
        if id:
            response = Response.objects.get(id=id)
            response.current = False
            response.save()
        else:
            pass


