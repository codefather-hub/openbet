RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y python3 python3-pip python-dev python3-dev
RUN pip3 install -r requirements.txt

python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py collectstatic
python3 manage.py runserver
